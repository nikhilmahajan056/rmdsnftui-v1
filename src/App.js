import { useEffect } from 'react';
import { useMoralis, useMoralisWeb3Api } from "react-moralis";
import Web3 from 'web3';

function App() {
  const { authenticate, isAuthenticated, isAuthenticating, user, account, logout } = useMoralis();
  const Web3Api = useMoralisWeb3Api();

    useEffect(() => {
      if (isAuthenticated) {
        fetchNFTsForNewContract();
        fetchNFTsForOldContract();
      }
    }, [isAuthenticated]);

    const fetchNFTsForNewContract = async () => {
      console.log("account is:", account);
      const options = {
        chain: process.env.REACT_APP_ETHEREUM_NETWORK,
        address: account,
        token_address: process.env.REACT_APP_NFT_NEW_CONTRACT_ADDRESS,
      };
      const allNFTs = await Web3Api.account.getNFTsForContract(options);
      console.log("new contract NFTs:", allNFTs); // use forEach and then use metadata -> image parameter to get the image URL
    };

    const transferNFT = async () => {
      const configResponse = await fetch("/config.json", {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      });
      const config = await configResponse.json();

      let web3 = new Web3(window.ethereum);
      const contract = new web3.eth.Contract(config.NFT.abi, config.NFT.contract_address);
      const resp = await contract.methods.transferFrom(account, "0x0e27964fC0f7ce4f8393A7154738Ba0f72b09164", 10).send({from: account});

      if (resp.status) {
        console.log("transaction successful!", resp.transactionHash);
      } else {
        console.error("unexpected error occurred:", resp.json())
      }
    }

    const fetchNFTsForOldContract = async () => {
      console.log("account is:", account);
      const options = {
        chain: process.env.REACT_APP_ETHEREUM_NETWORK,
        address: account,
        token_address: process.env.REACT_APP_NFT_OLD_CONTRACT_ADDRESS,
      };
      const allNFTs = await Web3Api.account.getNFTsForContract(options);
      console.log("old contract NFTs:", allNFTs); // use forEach and then use metadata -> image parameter to get the image URL
    };

    const swapTokens = async () => {
      const approved = approveTransfer();

      if (approved) {
        const configResponse = await fetch("/config.json", {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        });
        const config = await configResponse.json();
  
        let web3 = new Web3(window.ethereum);
        const contract = new web3.eth.Contract(config.tokenSwap.abi, config.tokenSwap.contract_address);
        const resp = await contract.methods.swapToken(4).send({from: account});
        
        if (resp.status) {
          console.log("transaction successful!", resp.transactionHash);
        } else {
          console.error("unexpected error occurred:", resp.json())
        }
      }
    }

    const approveTransfer = async () => {
      const configResponse = await fetch("/config.json", {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      });
      const config = await configResponse.json();

      let web3 = new Web3(window.ethereum);
      const contract = new web3.eth.Contract(config.NFT.abi, process.env.REACT_APP_NFT_OLD_CONTRACT_ADDRESS);
      const resp = await contract.methods.approve(config.tokenSwap.contract_address, 4).send({from: account});

      if (resp.status) {
        console.log("transaction successful!", resp.transactionHash);
        return true;
      } else {
        console.error("unexpected error occurred:", resp.json())
      }
    }

    const login = async () => {
      if (!isAuthenticated) {
        await authenticate({signingMessage: "Log in to dApp" })
          .then(function (user) {
            console.log("logged in user:", user);
            console.log(user.get("ethAddress"));
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    }

    const logOut = async () => {
      await logout();
      console.log("logged out");
    }

  return (
    <div>
      <h1>Moralis Hello World!</h1>
      <button onClick={login}>Moralis Metamask Login</button>
      <button onClick={logOut} disabled={isAuthenticating}>Logout</button>
      <button onClick={transferNFT}>Transfer NFT</button>
      <button onClick={swapTokens}>Swap Old NFT</button>
    </div>
  );
}

export default App;
